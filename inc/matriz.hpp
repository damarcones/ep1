#ifndef MATRIZ_HPP
#define MATRIZ_HPP

using namespace std;

class Matriz {
private:
    char casas[100][100];

public:
    Matriz();
    Matriz(int i, int j,char casas);

    int getI();
    void setI(int i);


    int getJ();
    void setJ(int j);


    char getCasas();
    void setCasas(char casas);





};
#endif
